<?php

/**
 * @file
 *
 * Plugin to provide to return 404 if page argument is invalid
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Page Argument 404'),
  'description' => t('Returns 404 if page argument is not applicable'),
  'callback' => 'panels_page_argument_404_ctools_access_check',
  'settings form' => 'panels_page_argument_404_ctools_access_settings',
  'summary' => 'panels_page_argument_404_ctools_access_summary',
);

function panels_page_argument_404_ctools_access_check($conf, $context) {
  if (arg(1)==NULL) {
    return TRUE;
  }
}

function panels_page_argument_404_ctools_access_settings($form, &$form_state, $conf) {
  return $form;
}

function panels_page_argument_404_ctools_access_summary($conf, $context) {
  return t('Returns 404 if page argument is not applicable');
}
